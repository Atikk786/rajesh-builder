// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig :{
    apiKey: "AIzaSyAqpxl__cZroRsNpaWPcrnVMMwpGn_XqHo",
    authDomain: "rajesh-builder.firebaseapp.com",
    projectId: "rajesh-builder",
    storageBucket: "rajesh-builder.appspot.com",
    messagingSenderId: "713235735676",
    appId: "1:713235735676:web:0520587f1ef36126e495f9",
    measurementId: "G-FDBC79ZZ9E"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
