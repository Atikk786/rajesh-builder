import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { MatDialogModule } from '@angular/material/dialog';
import { UserDialogComponent } from './user-dialog/user-dialog.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ForgotPasswordDialogComponent } from './forgot-password-dialog/forgot-password-dialog.component';
import { HttpClientModule } from '@angular/common/http';
// <<<<<<< HEAD
// import { ServiceWorkerModule } from '@angular/service-worker';
// import { environment } from '../environments/environment';
// =======
// import { AngularFirestoreModule } from '@angular/fire/firestore';
// import { AngularFireModule } from '@angular/fire';
// import { environment } from 'src/environments/environment';
// import { ServiceWorkerModule } from '@angular/service-worker';
// >>>>>>> 69053f9c84fc513c701d3aeb73beff6f6e2d8814


@NgModule({
  
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    UserDialogComponent,
    ProjectDetailComponent,
    ForgotPasswordDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule ,
    MatDialogModule,
    BrowserAnimationsModule,
    HttpClientModule,
// <<<<<<< HEAD
//     ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
// =======
//     AngularFireModule.initializeApp(environment.firebaseConfig),
//     AngularFirestoreModule,
//     ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })

// >>>>>>> 69053f9c84fc513c701d3aeb73beff6f6e2d8814
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
