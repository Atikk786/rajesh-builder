export class ProjectList {
    id:number;
    name:string;
    description:string;
    imgpath:string;
    location:string;
}