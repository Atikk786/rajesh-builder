export interface MasterSite {
    userId: number;
    id:     number;
    title:  string;
    body:   string;
    message:string;
}


export interface SitesProject{

    id:          string;
    city__name:    string;
    site_name:   string;
    image_url:   string;
    description: string;
    location:    string;
    start_date:  string;
    end_date:    string;

    // constructor(data:any){
    //     this.id = String(data['id']);
    //     this.cityName = String(data['city__name']);
    //     this.site_name = String(data['site_name']);
    //     this.image_url = String(data['image_url']);
    //     this.description = String(data['description']);
    //     this.location = String(data['location']);
    //     this.start_date = String(data['start_date']);
    //     this.end_date = String(data['end_date']);
    // }
}


