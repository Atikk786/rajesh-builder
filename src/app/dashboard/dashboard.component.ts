import { Component, OnInit } from '@angular/core';
import { ProjectList } from '../Models/ProjectList';
import { MatDialog } from '@angular/material/dialog';
import { UserDialogComponent } from './../user-dialog/user-dialog.component';
import { Service } from '../Services/Service';
import { SitesProject } from '../Models/MasterSites';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

  loop = [1, 2, 3, 4, 5]
  obj: ProjectList
  projectData: ProjectList[] = []
  imageList = ["assets/Mahaveer Recidancy.jpg",
    "assets/MAHAVIR KUNJ.jpg",
    "assets/KANCHAN BLISS.jpg",
    "assets/KANCHAN KUNJ.jpg",
    "assets/Building.jpg"];
  
  i = 0;
  sitesProject: SitesProject[];


  constructor(public dialog: MatDialog,private serviceObj: Service) {
      this.getSitesProjects();
    // for (let a in this.imageList) {
    //   this.obj = new ProjectList()
    //   this.obj.id = parseInt(a)
    //   this.obj.name = 'Sakib'
    //   this.obj.description = 'bsdfblkf fjkghlfkjg h  fdng kjl;d'
    //   this.obj.imgpath = "/assets/on_click.jpg"
    //   this.obj.location= ""
    //   this.projectData.push(this.obj)
    // }


  }

  getSitesProjects(){
    const response = this.serviceObj.getRequest('projects/')
    response.subscribe((data:SitesProject[]) => {
      this.sitesProject = data;  
    },err => {

    })
  }

  

  change() {

    const imageElement: HTMLImageElement = document.querySelector('#bannerID');

    if (this.i >= this.imageList.length - 1) {
      this.i = 0
    } else {
      this.i++;
    }
    imageElement.src = this.imageList[this.i];
  }

  ngOnInit(): void {
    this.i = 0;
    setInterval(() => { this.change(); }, 2000)
  }

  submitData() {
    console.log("You click me");
  }
  openDialog(index) {

    const dialogRef = this.dialog.open(UserDialogComponent, { data: this.projectData[index] });

  }



}