import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ForgotPasswordDialogComponent } from '../forgot-password-dialog/forgot-password-dialog.component';
import { MasterSite } from '../Models/MasterSites';
import { ProjectList } from '../Models/ProjectList';
import { Service } from '../Services/Service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input()
  result$: Observable<any>;
  masterList: MasterSite[] = [];


  str = 'Login'
  

  constructor(public dialog: MatDialog, private api: Service,public router:Router) {


  }

  ngOnInit(): void {

  }

  forgotPassword() {
    const dialogRef = this.dialog.open(ForgotPasswordDialogComponent, {
      height: '280px'

    });
  }
  validateLogin() {

    const email = (document.getElementById('email') as HTMLInputElement).value
    const password = (document.getElementById('password') as HTMLInputElement).value

    this.result$ = this.api.validateLogin(email,password);
    this.result$.subscribe((data) => {
      this.router.navigate(['/dashboard'])     
    }, err => {
      console.log(err);
    });
  }
}
