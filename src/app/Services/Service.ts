import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
providedIn:'root'
})

export class Service{
    baseUrl = "http://127.0.0.1:8000/api/"
    public header = {'Content-Type': 'application/json'};

    constructor(private httpClient?: HttpClient){
    
    }

    validateLogin(email,password): Observable<any>{
        const body = {
            "email":email,
            "password":password
        }       
        
        const respone = this.httpClient.post(this.baseUrl+"login/",body,{headers:this.header})
        // console.log(respone) 
        return respone;
    }   
    

    getRequest(controller:string):Observable<any>{
        const url = this.baseUrl+controller
        const response = this.httpClient.get(url,{headers:this.header})
        return response
    }

    postRequest(controller:string,params:any):Observable<any>{
        const url = this.baseUrl+controller
        const response = this.httpClient.post(url,params,{headers:this.header})
        return response
    }

}