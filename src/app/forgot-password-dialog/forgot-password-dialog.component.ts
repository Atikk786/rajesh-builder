import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgot-password-dialog',
  templateUrl: './forgot-password-dialog.component.html',
  styleUrls: ['./forgot-password-dialog.component.css']
})
export class ForgotPasswordDialogComponent implements OnInit {

  header="Reset Password"
  isValidationSuccessFul=false;
  placeHolder="Enter OTP";
  firstPlaceHolder='your@example.com';
  buttonName="Send OTP"

  constructor() { }

  ngOnInit(): void {
    const passwordField=document.getElementById('password');
    passwordField.style.display = 'none';
  }

  sendOTP(){
    if(!this.isValidationSuccessFul){
        this.isValidationSuccessFul = !this.isValidationSuccessFul;
        const passwordField=document.getElementById('password');
        passwordField.style.display = 'block';    
    }else{
      this.header="Enter Password";
      this.placeHolder = "Confirm Passowrd";
      this.firstPlaceHolder = 'New Password';
      this.buttonName="Submit"
    }
    
  }

}
