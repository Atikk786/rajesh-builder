import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Service } from './Services/Service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'rajesh-builder';

  constructor(private router: Router) {

  }


  login() {
    this.router.navigate(['/login'])
  }

}

