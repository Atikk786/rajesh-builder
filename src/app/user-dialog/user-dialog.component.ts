import { Component, OnInit ,Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ProjectList } from '../Models/ProjectList';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.css']
})
export class UserDialogComponent implements OnInit {

  selectedProject:ProjectList=null
  constructor( public dialogRef: MatDialogRef<UserDialogComponent>,@Inject(MAT_DIALOG_DATA) public selectedProjectValue: ProjectList) { 
    this.selectedProject = selectedProjectValue
  }

  ngOnInit(): void {
  }

}
